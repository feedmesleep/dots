#!/bin/bash

check_packages() {
    declare -a missing=()
    packages=('unzip' 'wget' 'curl' 'git' 'uwufetch' 'python-pywal')
    
    for package in "${packages[@]}"; do
        echo "$package"
        if ! pacman -Qs "$package" &>/dev/null; then
            missing+=("$package")
        fi
    done

    if [ ${#missing[@]} -eq 0 ]; then
        echo "All packages are available"
    else
        echo "The following packages are missing: "
        printf '%s\n' "${missing[@]}"
        echo "Installing them..."
        echo "Will need sudo password: "
        read -s pass

        sudo pacman -S "${missing[@]}" --noconfirm --needed
    fi
}

if [ -f /etc/os-release ]; then
    . /etc/os-release
    os=$NAME
    id=$ID
fi

check_packages

echo 'OhMyBash (omb) or OhMyPosh (omp)?'
read termprompt

if [ $termprompt = 'omb' ]; then
    bash -c '$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)'
elif [ $termprompt = 'omp' ]; then
    echo 'Making .posthemes (directory for theme files) and .bin (directory for local user packages) directories'
    mkdir $HOME/{.poshthemes,.bin}
    curl -s https://ohmyposh.dev/install.sh | bash -s -- -t $HOME/.poshthemes -d $HOME/.bin
    cp ../dots/.bashrc ~/ -f
    source ~/.bashrc
    oh-my-posh font install
fi
