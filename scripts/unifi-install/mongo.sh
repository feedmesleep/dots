#!/bin/bash

wget -qO - https://www.mongodb.org/static/pgp/server-3.0.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y mongodb-org

sudo systemctl enable --now mongod
