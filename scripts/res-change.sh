#!/bin/bash
# 1920x820 99.93 Hz (CVT) hsync: 87.04 kHz; pclk: 227.00 MHz
# For use in VM Distros requiring to have resolutions added for some reason
xrandr --newmode "1920x820_100.00"  227.00  1920 2064 2264 2608  820 823 833 871 -hsync +vsync
xrandr --addmode Virtual-1 1920x820_100.00
xrandr --output Virtual-1 --mode 1920x820_100.00
