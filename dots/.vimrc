set nocompatible
filetype on
filetype plugin on
filetype indent on

syntax on
set number

set shiftwidth=4
set tabstop=4
set expandtab

set incsearch
set ignorecase

set smartcase

set showcmd

set showmatch

set showmode

set hlsearch
set history=1000

" Enable auto completion menu after pressing TAB.
set wildmenu

" Make wildmenu behave like similar to Bash completion.
set wildmode=list:longest

" There are certain files that we would never want to edit with Vim.
" Wildmenu will ignore files with these extensions.
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx
