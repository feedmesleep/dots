cossh_func() {
    if [ -f ~/.ssh/id_rsa.pub ]; then
        cat ~/.ssh/id_rsa.pub | xclip -sel clip
    elif [ -f ~/.ssh/id_ecdsa.pub ]; then
        cat ~/.ssh/id_ecdsa.pub | xclip -sel clip
        cat ~/.ssh/id_ecdsa.pub
    else
        echo "No Public key file found".
    fi
}


#bash
alias edbash='vim ~/.bashrc'
alias srcbash="source ~/.bashrc"
alias edbasl="vim ~/.bash_aliases"

#system
alias ls='ls --color=never'
alias ll="ls -l"
alias lsc="ls --color=none"
alias llc="ls -l --color=none"
alias grep='grep --color=auto'

alias sys-update="sudo pacman -Sy"
alias sys-upgrade="sudo pacman -Su"

alias cossh='cat ~/.ssh/id_ecdsa.pub | xclip -sel clip'

#python
alias py="python3"
alias actpy="source ~/.python/bin/activate"

#dotnet
alias dBR="dotnet build; dotnet run"
alias dotRun="dotnet run"
alias dotBuild="dotnet build"

#System Monitor
alias duf="duf --only local,fuse"

#Backup
alias onebackup="bash ~/.scripts/onedrive-backup.sh"
alias dotbackup="cp -v ~/.bash_profile ~/.bash_aliases ~/.bashrc ~/Git/dots/dots/"
alias mvanime="nohup ~/.python/bin/python ~/Git/anime_related/move-anime.py > ~/.logs/move-anime.log 2>&1 < /dev/null &"
