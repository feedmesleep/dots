#
# ~/.bashrc
#
# bash file sourcing
if [[ -f ~/.bash_exports ]]; then
    source ~/.bash_exports
fi

if [[ -f ~/.bash_aliases ]]; then
    source ~/.bash_aliases
fi

(cat ~/.cache/wal/sequences &)
eval "(uwufetch)"
eval "$(oh-my-posh init bash --config /home/feedmesleep/.poshthemes/pure.omp.json)"
[[ $- != *i* ]] && return

# temp aliases
alias change-port='f() { sudo sed -i.bak "s|<InternalHttpPort>[0-9]*</InternalHttpPort>|<InternalHttpPort>$1</InternalHttpPort>|" "$2"; rm "$2.bak"; }; f'
